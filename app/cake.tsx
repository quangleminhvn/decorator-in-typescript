export interface Cake {
  price: number;
  printBasePrice(): string;
  printBill(): string;
}
// base export class cake
export class BaseCake implements Cake {
  price: number;

  constructor(num: number = 40) {
    this.price = num;
  }

  public printBasePrice(): string {
    return '<tr> <td>This cake like any normal cake, has milk, egg and flour .Price is</td> <td>' + this.price + '</td> </tr>'
  }

  public printBill(): string {
    return this.printBasePrice() + '<tr> <td>Total price is</td> <td>' + this.price + '</td> </tr>'
  }
}
// decoration from BaseCake
export class LargeCake extends BaseCake {
  constructor(num: number = 60) {
    super(num)
  }

  public printBasePrice(): string {
    return '<tr> <td>This cake like any normal cake, has milk, egg and flour .Large Cake price is</td> <td>' + this.price + '</td> </tr>'
  }
}
export class SmallCake extends BaseCake {
  constructor(num: number = 20) {
    super(num)
  }
  public printBasePrice(): string {
    return '<tr> <td>This cake like any normal cake, has milk, egg and flour .Small Cake price is</td> <td>' + this.price + '</td> </tr>'
  }
}
// Define Decoration Class
export class DecoratorCake implements Cake {
  price: number;
  private cake: Cake;
  private decorPrice: number;

  constructor( cake: Cake, num: number = 20 ) {
    this.decorPrice = num;
    this.cake = cake;
    this.price = this.decorPrice + this.cake.price
  }

  public printBasePrice(): string {
    return this.cake.printBasePrice();
  }

  public get getdecorPrice(): number {
    return this.decorPrice;
  }

  public printBill(): string {
    return this.printBasePrice() + '<tr> <td>Total price is</td> <td>' + this.price + '</td> </tr>'
  }
}

export class CakeCherry extends DecoratorCake {

  constructor(cake: Cake, num : number = 50) {
    super(cake, num);
  }

  public printBasePrice(): string {
    return super.printBasePrice() + '<tr> <td>With Cherry, price is</td> <td>' + this.getdecorPrice + '</td> </tr>';
  }
}

export class CakeScream extends DecoratorCake {

  constructor(cake: Cake, num : number = 30) {
    super(cake, num);
  }

  public printBasePrice(): string {
    return super.printBasePrice() + '<tr> <td>With Scream, price is</td> <td>' + this.getdecorPrice + '</td> </tr>';
  }
}
