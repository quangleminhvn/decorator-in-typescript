import $ = require('jquery');
import * as CakePattern from './cake';

$('#cake-form').submit(function(e){
  e.preventDefault();
  var decorator1: CakePattern.Cake;
  var res = $(this).serialize();
  switch (res) {

    case "size=0&decor=0":
      decorator1 = new CakePattern.BaseCake();
      break;

    case "size=small&decor=0":
      decorator1 = new CakePattern.SmallCake();
      break;

    case "size=large&decor=0":
      decorator1 = new CakePattern.LargeCake();
      break;

    case "size=0&decor=cherry":
      decorator1 = new CakePattern.CakeCherry( new CakePattern.BaseCake());
      break;

    case "size=0&decor=scream":
      decorator1 = new CakePattern.CakeScream( new CakePattern.BaseCake());
      break;

    case "size=small&decor=cherry":
      decorator1 = new CakePattern.CakeCherry( new CakePattern.SmallCake());
      break;

    case "size=small&decor=scream":
      decorator1 = new CakePattern.CakeScream( new CakePattern.SmallCake());
      break;

    case "size=large&decor=cherry":
      decorator1 = new CakePattern.CakeCherry( new CakePattern.LargeCake());
      break;

    case "size=large&decor=scream":
      decorator1 = new CakePattern.CakeScream( new CakePattern.LargeCake());
      break;

    default:
      break;
  }
  $('#result').html(decorator1.printBill());
});

